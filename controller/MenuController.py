from controller.BaseController import BaseController


class MenuController(BaseController):

    def start_clicked(self):
        self.navigator.show_levels()

    def how_to_play_clicked(self):
        pass

    def exit_clicked(self):
        self.navigator.exit()
