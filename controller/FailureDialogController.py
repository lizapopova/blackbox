from controller.BaseController import BaseController


class FailureDialogController(BaseController):

    def show_failure(self, answer: int, input: int, output: int):
        self.view.show_result(answer, input, output)

    def levels_clicked(self):
        self.navigator.show_levels()
        self.navigator.hide_failure_dialog()

    def continue_clicked(self):
        self.navigator.hide_failure_dialog()
