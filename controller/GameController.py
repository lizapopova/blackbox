from controller.BaseController import BaseController
from model.Formatter import format_input
from model.Levels import transformers


class GameController(BaseController):

    def __init__(self, navigator):
        super().__init__(navigator)
        self.level: int = -1
        self.input_text: str = ""
        self.attempts = 0

    def start_game(self, level: int):
        self.level = level
        self.view.set_title(f"Level {self.level}")
        self.__update_input("")
        self.__update_attempts_cnt(0)
        self.view.clear_attempt_list()

    def back_clicked(self):
        self.navigator.show_levels()

    def digit_entered(self, digit: int):
        self.__update_input(self.input_text + str(digit))

    def del_clicked(self):
        self.__update_input(self.input_text[:-1])

    def clr_clicked(self):
        self.__update_input("")

    def text_edited(self, new_text):
        self.__update_input(new_text)

    def enter_triggered(self):
        if self.input_text == "": return
        input = int(self.input_text)
        output = transformers[self.level - 1](input)
        print(f"{input} -> {output}")
        self.__update_attempts_cnt(self.attempts + 1)
        self.__update_input("")
        self.view.add_attempt(input, output)

    def hint_clicked(self):
        self.navigator.show_hint(self.level)

    def test_clicked(self):
        self.navigator.start_test(self.level)

    def __update_attempts_cnt(self, cnt: int):
        self.attempts = cnt
        self.view.set_attempts_cnt(cnt)

    def __update_input(self, text: str):
        self.input_text = format_input(text)
        self.view.set_input_text(self.input_text)
