from controller.BaseController import BaseController
from model.Levels import hints


class HintDialogController(BaseController):

    def show_hint(self, level: int):
        self.view.show_hint(hints[level - 1])

    def continue_clicked(self):
        self.navigator.hide_hint_dialog()
