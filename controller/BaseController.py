class BaseController:

    def __init__(self, navigator):
        self.navigator = navigator
        # Should be set later
        self.view = None
