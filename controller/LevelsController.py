from controller.BaseController import BaseController


class LevelsController(BaseController):

    def back_clicked(self):
        self.navigator.show_menu()

    def level_clicked(self, level: int):
        self.navigator.start_game(level)
