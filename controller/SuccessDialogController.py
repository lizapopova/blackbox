from controller.BaseController import BaseController
from model.Levels import level_cnt


class SuccessDialogController(BaseController):

    def __init__(self, navigator):
        super().__init__(navigator)
        self.level: int = -1

    def show_success(self, level: int):
        self.level = level

    def levels_clicked(self):
        self.navigator.show_levels()
        self.navigator.hide_success_dialog()

    def next_clicked(self):
        if self.level < (level_cnt - 1):
            self.navigator.start_game(self.level + 1)
        else:
            self.navigator.show_levels()
        self.navigator.hide_success_dialog()
