from controller.BaseController import BaseController
from model.Formatter import format_input
from model.Generator import generate_tests, test_cnt
from model.Levels import transformers


class TestDialogController(BaseController):

    def __init__(self, navigator):
        super().__init__(navigator)
        self.level: int = -1
        self.input_text: str = ""
        self.question_number = 0
        self.tests: [int] = []

    def start_test(self, level: int):
        self.level = level
        self.tests = generate_tests()
        self.__prepare_test(0)

    def text_edited(self, new_text):
        self.__update_input(new_text)

    def cancel_clicked(self):
        self.navigator.hide_test_dialog()

    def answer_triggered(self):
        if self.input_text == "": return
        input = self.tests[self.question_number]
        answer = int(self.input_text)
        output = transformers[self.level - 1](input)
        print(f"{answer}, {input} -> {output}")
        if answer == output:
            if self.question_number == (test_cnt - 1):
                self.navigator.show_success_dialog(self.level)
                self.navigator.hide_test_dialog()
            else:
                self.__prepare_test(self.question_number + 1)
        else:
            self.navigator.show_failure_dialog(answer, input, output)
            self.navigator.hide_test_dialog()

    def __update_input(self, text: str):
        self.input_text = format_input(text)
        self.view.set_input_text(self.input_text)

    def __update_question_number(self, number):
        self.question_number = number
        self.view.set_question_number(number + 1)

    def __prepare_test(self, number: int):
        self.__update_question_number(number)
        self.__update_input("")
        self.view.set_test_input(self.tests[number])

