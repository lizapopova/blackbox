from PySide2.QtWidgets import QWidget, QPushButton

from controller.LevelsController import LevelsController
from view.BaseView import BaseView, make_callback


class LevelsView(BaseView):

    # noinspection PyTypeChecker
    def __init__(self, widget: QWidget, controller: LevelsController):
        super().__init__(widget, controller)

        self.back_button: QPushButton = self.widget.findChild(QPushButton, "backButton")
        self.back_button.clicked.connect(self.controller.back_clicked)

        self.level_buttons: [QPushButton] = []
        for level in range(0, 24):
            self.level_buttons.append(self.widget.findChild(QPushButton, f"level{level + 1}Button"))
            self.level_buttons[level].clicked.connect(make_callback(self.controller.level_clicked, level + 1))
