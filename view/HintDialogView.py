from PySide2.QtWidgets import QWidget, QPushButton, QLabel

from controller.HintDialogController import HintDialogController
from view.BaseView import BaseView


class HintDialogView(BaseView):

    # noinspection PyTypeChecker
    def __init__(self, widget: QWidget, controller: HintDialogController):
        super().__init__(widget, controller)

        self.hint_label: QLabel = self.widget.findChild(QLabel, "hintLabel")

        self.continue_button: QPushButton = self.widget.findChild(QPushButton, "continueButton")
        self.continue_button.clicked.connect(self.controller.continue_clicked)

    def show_hint(self, hint: str):
        self.hint_label.setText(hint)
