from PySide2.QtWidgets import QWidget
from controller import BaseController


class BaseView:

    def __init__(self, widget: QWidget, controller: BaseController):
        self.widget = widget
        self.controller = controller


def make_callback(func, *param):
    return lambda: func(*param)
