from PySide2.QtWidgets import QWidget, QPushButton
from view.BaseView import BaseView
from controller import MenuController


class MenuView(BaseView):

    # noinspection PyTypeChecker
    def __init__(self, widget: QWidget, controller: MenuController):
        super().__init__(widget, controller)

        self.start_button: QPushButton = self.widget.findChild(QPushButton, "startButton")
        self.start_button.clicked.connect(self.controller.start_clicked)

        self.help_button: QPushButton = self.widget.findChild(QPushButton, "howToPlayButton")
        self.help_button.clicked.connect(self.controller.how_to_play_clicked)

        self.exit_button: QPushButton = self.widget.findChild(QPushButton, "exitButton")
        self.exit_button.clicked.connect(self.controller.exit_clicked)