from PySide2.QtWidgets import QWidget, QPushButton

from controller.SuccessDialogController import SuccessDialogController
from view.BaseView import BaseView


class SuccessDialogView(BaseView):

    # noinspection PyTypeChecker
    def __init__(self, widget: QWidget, controller: SuccessDialogController):
        super().__init__(widget, controller)

        self.levels_button: QPushButton = self.widget.findChild(QPushButton, "levelsButton")
        self.levels_button.clicked.connect(self.controller.levels_clicked)
        self.next_button: QPushButton = self.widget.findChild(QPushButton, "nextButton")
        self.next_button.clicked.connect(self.controller.next_clicked)
