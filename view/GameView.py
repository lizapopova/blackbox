from PySide2.QtGui import QStandardItemModel, QStandardItem, QFont, QBrush, QColor
from PySide2.QtWidgets import QWidget, QPushButton, QLabel, QLineEdit, QListView

from controller.GameController import GameController
from view.BaseView import BaseView, make_callback


class GameView(BaseView):

    # noinspection PyTypeChecker
    def __init__(self, widget: QWidget, controller: GameController):
        super().__init__(widget, controller)

        self.title_label: QLabel = self.widget.findChild(QLabel, "titleLabel")
        self.back_button: QPushButton = self.widget.findChild(QPushButton, "backButton")
        self.back_button.clicked.connect(self.controller.back_clicked)

        # Keyboard actions
        self.digit_buttons: [QPushButton] = []
        for digit in range(0, 10):
            self.digit_buttons.append(self.widget.findChild(QPushButton, f"keyboardButton{digit}"))
            self.digit_buttons[digit].clicked.connect(make_callback(self.controller.digit_entered, digit))
        self.del_button: QPushButton = self.widget.findChild(QPushButton, "keyboardButtonDel")
        self.del_button.clicked.connect(self.controller.del_clicked)
        self.clr_button: QPushButton = self.widget.findChild(QPushButton, "keyboardButtonClr")
        self.clr_button.clicked.connect(self.controller.clr_clicked)

        self.input_field: QLineEdit = self.widget.findChild(QLineEdit, "inputField")
        self.input_field.textEdited.connect(self.controller.text_edited)
        self.input_field.cursorPositionChanged.connect(self.__return_cursor_to_end)
        self.input_field.returnPressed.connect(self.controller.enter_triggered)

        self.hint_button: QPushButton = self.widget.findChild(QPushButton, "hintButton")
        self.hint_button.clicked.connect(self.controller.hint_clicked)
        self.test_button: QPushButton = self.widget.findChild(QPushButton, "testButton")
        self.test_button.clicked.connect(self.controller.test_clicked)
        self.enter_button: QPushButton = self.widget.findChild(QPushButton, "enterButton")
        self.enter_button.clicked.connect(self.controller.enter_triggered)

        self.attempts_label: QLabel = self.widget.findChild(QLabel, "attemptsLabel")

        self.attempt_list: QListView = self.widget.findChild(QListView, "attemptListView")
        self.attempt_list_model = QStandardItemModel(self.attempt_list)
        self.attempt_list.setModel(self.attempt_list_model)

    def set_title(self, title: str):
        self.title_label.setText(title)

    def set_input_text(self, text: str):
        self.input_field.setText(text)
        self.input_field.setFocus()

    def set_attempts_cnt(self, cnt: int):
        self.attempts_label.setText(f"Attempts: {cnt}")

    def clear_attempt_list(self):
        self.attempt_list_model.clear()

    def add_attempt(self, input: int, output: int):
        item = self.__create_item(input, output)
        self.attempt_list_model.appendRow(item)
        self.attempt_list.scrollToBottom()

    def __create_item(self, input: int, output: int) -> QStandardItem:
        item = QStandardItem(f"{input} -> {output}")
        font = QFont()
        font.setPixelSize(25)
        font.setFamily('Ubuntu Mono')
        item.setFont(font)
        item.setSelectable(False)
        item.setForeground(QBrush(QColor(255, 255, 255)))
        return item

    def __return_cursor_to_end(self):
        self.input_field.setCursorPosition(len(self.input_field.text()))
