from PySide2.QtWidgets import QWidget, QPushButton, QLabel

from controller.FailureDialogController import FailureDialogController
from view.BaseView import BaseView


class FailureDialogView(BaseView):

    # noinspection PyTypeChecker
    def __init__(self, widget: QWidget, controller: FailureDialogController):
        super().__init__(widget, controller)

        self.answer_label: QLabel = self.widget.findChild(QLabel, "answerLabel")
        self.test_label: QLabel = self.widget.findChild(QLabel, "testLabel")

        self.levels_button: QPushButton = self.widget.findChild(QPushButton, "levelsButton")
        self.levels_button.clicked.connect(self.controller.levels_clicked)
        self.continue_button: QPushButton = self.widget.findChild(QPushButton, "continueButton")
        self.continue_button.clicked.connect(self.controller.continue_clicked)

    def show_result(self, answer: int, input: int, output: int):
        self.answer_label.setText(f"You answer is {answer}, correct one is")
        self.test_label.setText(f"{input} -> {output}")