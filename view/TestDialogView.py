from PySide2.QtWidgets import QWidget, QPushButton, QLabel, QLineEdit

from controller.TestDialogController import TestDialogController
from view.BaseView import BaseView


class TestDialogView(BaseView):

    # noinspection PyTypeChecker
    def __init__(self, widget: QWidget, controller: TestDialogController):
        super().__init__(widget, controller)

        self.question_label: QLabel = self.widget.findChild(QLabel, "questionLabel")
        self.test_label: QLabel = self.widget.findChild(QLabel, "testLabel")

        self.input_field: QLineEdit = self.widget.findChild(QLineEdit, "answerInputField")
        self.input_field.textEdited.connect(self.controller.text_edited)
        self.input_field.cursorPositionChanged.connect(self.__return_cursor_to_end)
        self.input_field.returnPressed.connect(self.controller.answer_triggered)

        self.cancel_button: QPushButton = self.widget.findChild(QPushButton, "cancelButton")
        self.cancel_button.clicked.connect(self.controller.cancel_clicked)
        self.answer_button: QPushButton = self.widget.findChild(QPushButton, "answerButton")
        self.answer_button.clicked.connect(self.controller.answer_triggered)

    def set_question_number(self, number: int):
        self.question_label.setText(f"Question {number}/3")

    def set_test_input(self, input: int):
        self.test_label.setText(f"{input} -> ?")

    def set_input_text(self, text: str):
        self.input_field.setText(text)
        self.input_field.setFocus()

    def __return_cursor_to_end(self):
        self.input_field.setCursorPosition(len(self.input_field.text()))
