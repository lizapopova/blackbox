from model.Generator import max_value


# Number of digits
def transform_4(input: int) -> int:
    if input == 0:
        return 1
    ans = 0
    while input > 0:
        ans += 1
        input //= 10
    return ans


# Reverse number
def transform_5(input: int) -> int:
    return int(str(input)[::-1])


# Sum of digits
def transform_6(input: int) -> int:
    return sum(map(int, str(input)))


# Add number depending on the parity
def transform_7(input: int) -> int:
    if input % 2 == 0:
        return input + 5
    else:
        return input + 10


# Sort digits
def transform_9(input: int) -> int:
    digits = sorted(map(int, str(input)))
    return int("".join(map(str, digits)))


# Number of oval elements
def transform_10(input: int) -> int:
    ans = 0
    for c in str(input):
        if c == '0':
            ans += 1
        elif c == '6':
            ans += 1
        elif c == '8':
            ans += 2
        elif c == '9':
            ans += 1
    return ans


# Complement to 999999
def transform_11(input: int) -> int:
    return max_value - input


# Swap first and last digits
def transform_12(input: int) -> int:
    if input < 10:
        return input
    s = str(input)
    return int(s[-1:] + s[1:-1] + s[0])


# Number of 1 in binary
def transform_13(input: int) -> int:
    return sum(map(int, bin(input)[2:]))


# Absolute difference between first and last digits
def transform_14(input: int) -> int:
    first = int(str(input)[0])
    last = int(str(input)[-1])
    return abs(first - last)


# Absolute difference between number and reversed number
def transform_15(input: int) -> int:
    return abs(input - transform_5(input))


# Multiplication of all digits
def transform_16(input: int) -> int:
    ans = 1
    for c in map(int, str(input)):
        ans *= c
    return ans


# Max k that 2 ** k divides input
def transform_17(input: int) -> int:
    ans = 0
    while input % 2 == 0:
        ans += 1
        input //= 2
    return ans


# Closest square number
def transform_18(input: int) -> int:
    sq = int(input ** 0.5)
    abs1 = input - sq ** 2
    abs2 = (sq + 1) ** 2 - input
    return sq ** 2 if (abs1 < abs2) else (sq + 1) ** 2


# Multiply first digit by reminder
def transform_20(input: int) -> int:
    if input < 10:
        return input
    first = int(str(input)[0])
    r = int(str(input)[1:])
    return r * first


# Each digit replaced with complement to 9
def transform_21(input: int) -> int:
    return int("".join(str(9 - int(x)) for x in str(input)))


transformers = [
    lambda x: x + 1,
    lambda x: x * 2,
    lambda x: x // 3 + 1,
    lambda x: x % 7,
    transform_4,
    transform_5,
    transform_6,
    transform_7,
    lambda x: x ** 2 + x + 1,
    transform_9,
    transform_10,
    transform_11,
    transform_12,
    transform_13,
    transform_14,
    transform_15,
    transform_16,
    transform_17,
    transform_18,
    lambda x: int(x ** (1/3)),
    transform_20,
    transform_21,
    lambda x: x ** 2 - x,
    lambda x: 42
]

hints = [
    "Think of basic math operations",
    "Think of basic math operations",
    "Think of basic math operations",
    "Think of the result of division",
    "Think of the digits",
    "Think of the digits order",
    "Think of the digits",
    "Think of the number parity",
    "Think of square polynomial",
    "Think of the digits order",
    "Think of the digits shape",
    "Think of subtraction",
    "Think of the digits order",
    "Think of binary representation",
    "Think of absolute difference",
    "Think of absolute difference",
    "Think of multiplication",
    "Think of division by two",
    "Think of square of number",
    "Think of root of number",
    "Think of multiplication",
    "Think of complement",
    "Think of squaring",
    "Think of life"
]

level_cnt = len(transformers)
