def format_input(text: str) -> str:
    only_digits = ''.join(char for char in text if char.isdigit())
    if only_digits != "":
        # Remove trailing zeroes
        only_digits = str(int(only_digits))
    return only_digits
