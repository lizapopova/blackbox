import random

test_cnt = 3
max_value = 999_999


def generate_test() -> int:
    return random.randint(0, max_value)


def generate_tests() -> []:
    return [generate_test() for _ in range(test_cnt)]
