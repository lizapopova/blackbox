import sys

from PySide2 import QtWidgets
from PySide2.QtCore import QFile, QSize
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QMainWindow, QWidget, QStackedWidget

from controller.FailureDialogController import FailureDialogController
from controller.GameController import GameController
from controller.HintDialogController import HintDialogController
from controller.LevelsController import LevelsController
from controller.MenuController import MenuController
from controller.SuccessDialogController import SuccessDialogController
from controller.TestDialogController import TestDialogController
from view.FailureDialogView import FailureDialogView

from view.GameView import GameView
from view.HintDialogView import HintDialogView
from view.MenuView import MenuView
from view.LevelsView import LevelsView
from view.SuccessDialogView import SuccessDialogView
from view.TestDialogView import TestDialogView


def load_ui_widget(file_name: str) -> QWidget:
    file = QFile(file_name)
    file.open(QFile.ReadOnly)
    ui_widget = QUiLoader().load(file)
    file.close()
    return ui_widget


class AppWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.menu_controller = MenuController(self)
        self.menu_widget = load_ui_widget("ui/menu.ui")
        self.menu_view = MenuView(self.menu_widget, self.menu_controller)
        self.menu_controller.view = self.menu_view

        self.levels_controller = LevelsController(self)
        self.levels_widget = load_ui_widget("ui/levels.ui")
        self.levels_view = LevelsView(self.levels_widget, self.levels_controller)
        self.levels_controller.view = self.levels_view

        self.game_controller = GameController(self)
        self.game_widget = load_ui_widget("ui/game.ui")
        self.game_view = GameView(self.game_widget, self.game_controller)
        self.game_controller.view = self.game_view

        self.test_dialog_controller = TestDialogController(self)
        self.test_dialog = load_ui_widget("ui/test_dialog.ui")
        self.test_dialog_view = TestDialogView(self.test_dialog, self.test_dialog_controller)
        self.test_dialog_controller.view = self.test_dialog_view

        self.success_dialog_controller = SuccessDialogController(self)
        self.success_dialog = load_ui_widget("ui/success_dialog.ui")
        self.success_dialog_view = SuccessDialogView(self.success_dialog, self.success_dialog_controller)
        self.success_dialog_controller.view = self.success_dialog_view

        self.failure_dialog_controller = FailureDialogController(self)
        self.failure_dialog = load_ui_widget("ui/failure_dialog.ui")
        self.failure_dialog_view = FailureDialogView(self.failure_dialog, self.failure_dialog_controller)
        self.failure_dialog_controller.view = self.failure_dialog_view

        self.hint_dialog_controller = HintDialogController(self)
        self.hint_dialog = load_ui_widget("ui/hint_dialog.ui")
        self.hint_dialog_view = HintDialogView(self.hint_dialog, self.hint_dialog_controller)
        self.hint_dialog_controller.view = self.hint_dialog_view

        self.navigation: QStackedWidget = load_ui_widget("ui/navigation.ui")
        self.setCentralWidget(self.navigation)
        self.navigation.addWidget(self.menu_widget)
        self.navigation.addWidget(self.levels_widget)
        self.navigation.addWidget(self.game_widget)

        self.show_menu()

    def exit(self):
        QtWidgets.QApplication.quit()

    def show_menu(self):
        self.navigation.setCurrentIndex(0)

    def show_levels(self):
        self.navigation.setCurrentIndex(1)

    def start_game(self, level: int):
        self.game_controller.start_game(level)
        self.navigation.setCurrentIndex(2)

    def start_test(self, level: int):
        self.test_dialog_controller.start_test(level)
        self.test_dialog.show()

    def hide_test_dialog(self):
        self.test_dialog.hide()

    def show_success_dialog(self, level: int):
        self.success_dialog_controller.show_success(level)
        self.success_dialog.show()

    def hide_success_dialog(self):
        self.success_dialog.hide()

    def show_failure_dialog(self, answer: int, input: int, output: int):
        self.failure_dialog_controller.show_failure(answer, input, output)
        self.failure_dialog.show()

    def hide_failure_dialog(self):
        self.failure_dialog.hide()

    def show_hint(self, level: int):
        self.hint_dialog_controller.show_hint(level)
        self.hint_dialog.show()

    def hide_hint_dialog(self):
        self.hint_dialog.hide()


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    window = AppWindow()
    window.setFixedSize(QSize(900, 700))
    window.setWindowTitle("BlackBox")
    window.show()
    sys.exit(app.exec_())
